import 'package:flutter/material.dart';

class TeaCard extends StatelessWidget {
  TeaCard({@required this.teaName});

  final teaName;

  @override
  Widget build(BuildContext context) {
    return Card(
        margin: EdgeInsets.all(0),
        elevation: 10,
        shape: Border(bottom: BorderSide(color: Color.fromARGB(10, 0, 0, 0))),
        child: Padding(
            padding: EdgeInsets.only(top: 10, bottom: 10),
            child: Center(
                child: Text(teaName,
                    style: TextStyle(
                        fontSize: 18, fontWeight: FontWeight.bold)))));
  }
}
