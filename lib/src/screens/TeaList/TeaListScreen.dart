import 'package:flutter/material.dart';
import 'package:teforia/src/components/Search.dart';

import 'package:teforia/src/models/Tea.dart';
import 'package:teforia/src/services/Tea/TeaService.dart';
import 'package:teforia/src/screens/TeaList/components/TeaCard.dart';

class TeaListScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => TeaListScreenState();
}

class TeaListScreenState extends State<TeaListScreen> {
  var teas = [];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Tea list", style: TextStyle(color: Colors.black)),
          backgroundColor: Colors.white,
          elevation: 5,
          bottom: PreferredSize(
              child: Center(child: Search()),
              preferredSize: Size.fromHeight(50)),
        ),
        body: Padding(
          padding: EdgeInsets.only(top: 5),
          child: Container(
              color: Color.fromARGB(20, 0, 0, 0),
              child: FutureBuilder<List<Tea>>(
                future: TeaService.getTeas(),
                builder: (futureBuilderContext, snapshot) {
                  if (snapshot.hasData) {
                    return ListView.builder(
                        itemCount: snapshot.data.length,
                        itemBuilder: (listViewBuilderContext, index) {
                          return TeaCard(teaName: snapshot.data[index].name);
                        });
                  } else if (snapshot.hasError) {
                    return Text("${snapshot.error}");
                  }
                  return CircularProgressIndicator();
                },
              )),
        ));
  }
}
