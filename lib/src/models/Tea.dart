class Tea {
  final int id;
  final String name;

  Tea({this.id, this.name});

  factory Tea.fromJson(Map<String, dynamic> json) {
    return Tea(id: json["id"], name: json["name"]);
  }
}
