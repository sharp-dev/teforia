import 'package:flutter/material.dart';
import 'package:teforia/src/components/TabBarButton.dart';
import 'package:teforia/src/screens/TeaList/TeaListScreen.dart';

class TeforiaApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Teforia',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: SafeArea(
          child: DefaultTabController(
              length: 1,
              child: Scaffold(
                bottomNavigationBar: Material(
                    color: Colors.white,
                    child: TabBar(tabs: [
                      TabBarButton(iconData: Icons.details, text: "TEAS"),
                    ])),
                body: TabBarView(children: [
                  TeaListScreen(),
                ]),
              ))),
    );
  }
}
