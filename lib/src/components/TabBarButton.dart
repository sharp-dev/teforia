import 'package:flutter/material.dart';

class TabBarButton extends StatelessWidget {
  TabBarButton({@required this.iconData, @required this.text});

  final IconData iconData;
  final String text;

  @override
  Widget build(BuildContext context) {
    return Tab(
        child: Column(
      children: <Widget>[
        Icon(iconData, color: Colors.black),
        Text(text, style: TextStyle(color: Colors.black))
      ],
    ));
  }
}
