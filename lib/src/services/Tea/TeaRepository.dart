import 'dart:io';

import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';

class TeaRepository {
  TeaRepository._();
  static final TeaRepository repo = TeaRepository._();

  static Database _database;

  Future<Database> get database async {
    if (_database != null)
      return _database;

    _database = await initDB();
    return _database;
  }

  initDB() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, "TeaDB.db");
    return await openDatabase(path, version: 1, onOpen: (db) {}, onCreate: (db, version) async {
      await db.execute("CREATE TABLE Tea ("
          "id INTEGER PRIMARY KEY,"
          "name TEXT"
          ")");
    });
  }

  newTea(String name) async {
    final db = await database;
    var table = await db.rawQuery("SELECT MAX(id)+1 as id FROM Tea");
    int id = table.first["id"];
    var raw = await db.rawInsert(
        "INSERT Into Tea (id,name)"
            " VALUES (?,?)",
        [id, name]);
    return raw;
  }

  updateTea(int id, String name) async {
    final db = await database;
    var res = await db.update("Tea", {"id": id, "name": name},
        where: "id = ?", whereArgs: [id]);
    return res;
  }

  getTea(int id) async {
    final db = await database;
    var res = await db.query("Tea", where: "id = ?", whereArgs: [id]);
    return res.isNotEmpty ? res.first : null;
  }

  Future<List<String>> getAllTea() async {
    final db = await database;
    var res = await db.query("Tea");
    List<String> list =
    res.isNotEmpty ? res.map((c) => c["name"] as String).toList() : [];
    return list;
  }

  deleteTea(int id) async {
    final db = await database;
    return db.delete("Tea", where: "id = ?", whereArgs: [id]);
  }

  deleteAll() async {
    final db = await database;
    db.rawDelete("Delete * from Tea");
  }
}