import 'package:teforia/src/constants.dart';
import 'package:teforia/src/models/Tea.dart';
import 'package:teforia/src/services/Rest.dart';

class TeaService {
  static Future<List<Tea>> getTeas() async {
    var rootUrl = Config.rootUrl;
    var teaEndpoint = Endpoints.teas;
    var request = await Rest.getJson("$rootUrl/$teaEndpoint");
    List<dynamic> teasDto = request["teas"];
    List<Tea> teasModel = [];
    for (var i = 0; i < teasDto.length; i++) {
      teasModel.add(Tea.fromJson(teasDto[i]));
    }
    return teasModel;
  }
}
