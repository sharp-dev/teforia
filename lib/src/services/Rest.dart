import 'dart:convert';

import 'package:http/http.dart';

class Rest {
  static getJson(url) async {
    var request = await get(url);
    if (request.statusCode == 200) {
      return json.decode(request.body);
    } else {
      throw Exception("Failed to load json");
    }
  }
}
