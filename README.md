# teforia

Teforia

## Getting Started

This project is a starting point for a Flutter application.
It provides right source structure, and contains implementation for 
1) persistance
2) REST communication; Tea list is provided by dummy service at http://193.124.114.46:3001/teas
3) UI component for tea listing

The test application run on Android and iOs devices. 

### Start app
`flutter run`

### How the application/source code would handle phone vs. tablet form factor if we needed to have different layouts.
* The simplest way to do it - do it with the `MediaQuery` for fetching information about screen size, device pixel ratio, etc
* use [flutter_device_type](https://pub.dartlang.org/packages/flutter_device_type)
* use `AspectRatio`

### The list of Flutter plugins that the application would use
* [http](https://pub.dartlang.org/packages/http)
* [sqflite](https://pub.dartlang.org/packages/sqflite)
* [path_provider](https://pub.dartlang.org/packages/path_provider)
* [flutter_blue](https://github.com/pauldemarco/flutter_blue)
* [flutter_staggered_grid_view](https://pub.dartlang.org/packages/flutter_staggered_grid_view)

### Toolchain, build, push, test of the application
Flutter support this out-of-the-box.

### How the data (retrieved from cloud, any application specific data, etc.) would be persisted.
`TeaRepository` provides PoC version of SQLite database. `TeaService` - for the API fetching.

### Description of the documentation aspect of the project
Right now codebase is selfdocumented, but comment would be written for the complex things and components in future.
Complex UI workflow, communication protocols, complex algorithms should be documented by diagrams with description as an external documentation.